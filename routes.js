const path = require('path')

const component = (cmp) => {
  if (path.sep === '/') {
    // lin
    return path.resolve(__dirname, cmp)
  }

  // win
  return path.resolve(__dirname, cmp)
    .split(path.sep)
    .join('\\\\')
}
module.exports = [

  { // main page
    name: 'dsf',
    path: '/dsf',
    component: component('page/dsf.vue')
  }
]
